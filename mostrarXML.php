<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <title>Leer un documento XML</title>
    <link type="text/css" rel="stylesheet" href="css/estilos.css" />
</head>

<body>

    <?php 
        // Carga el documento XML con simplexml_load_file() 
        // Devuelve un objeto de la clase simplexml_element. 
        // o false, si hay un error (documento XML mal anidado, por ejemplo) 

        $xml = simplexml_load_file('registro.xml'); 

        if (!$xml) { 
            exit; 
        } 

        echo "<strong>Datos del XML</strong><br />\n"; 

        foreach ($xml->registro as $registro){
        printf("%s%'.20s%'.20s%'.20s%'.20s<br />\n", 
            $registro->nombre, 
            $registro->apellido, 
            $registro->edad, 
             
             
        }
    ?>

</body>
</html>